# Maintainer: Austcool-Walker <0ajwalker@gmail.com>
# Contributor: Austcool-Walker <0ajwalker@gmail.com>

pkgname=imagemagick-ubuntu
_pkgname=imagemagick
pkgver=6.9.7.4+dfsg
patchver=6.9.7.4+dfsg-16ubuntu6.8
pkgrel=1
pkgdesc="An image viewing/manipulation program"
url="https://www.imagemagick.org/"
arch=(x86_64)
license=(custom)
depends=(autotrace libltdl lcms2 fontconfig libxext liblqr libraqm libpng libxml2)
makedepends=(ghostscript openexr libwmf librsvg libxml2 openjpeg2 libraw opencl-headers libwebp
             chrpath ocl-icd glu ghostpcl ghostxps libheif jbigkit lcms2 libxext liblqr libraqm libpng autotrace)
checkdepends=(gsfonts)
optdepends=('ghostscript: PS/PDF support'
              'libheif: HEIF support'
              'libraw: DNG support'
              'librsvg: SVG support'
              'libwebp: WEBP support'
              'libwmf: WMF support'
              'libxml2: Magick Scripting Language' 
              'ocl-icd: OpenCL support'
              'openexr: OpenEXR support'
              'openjpeg2: JPEG2000 support'
              'pango: Text rendering'
              'imagemagick-doc: manual and API docs')
options=(!emptydirs libtool)
backup=(etc/$_relname/{colors,delegates,log,mime,policy,quantization-table,thresholds,type,type-{dejavu,ghostscript}}.xml)
conflicts=(imagemagick6 imagemagick)
provides=(libmagick libmagick6 imagemagick-ubuntu)
replaces=(imagemagick6 imagemagick libmagick libmagick6)
source=("http://archive.ubuntu.com/ubuntu/pool/main/i/${_pkgname}/${_pkgname}_${pkgver}.orig.tar.xz"
	"http://archive.ubuntu.com/ubuntu/pool/main/i/${_pkgname}/${_pkgname}_${patchver}.debian.tar.xz")
sha256sums=('47fb2cdd26f5913318c4504f16ea363e04d1f400dda9ec52e461ab661d724026'
            'cba99dbf3a5979e86b4020ad65d7208ae78f36ceb05aade0fc1a581410787508')

shopt -s extglob

prepare() {
  cd "$srcdir"/ImageMagick-6.9.7-4
  mkdir -p docpkg/usr/share
#  cp -v "${BUILDDIR}/series" "${srcdir}/debian/patches/series"
      for i in $(grep -v '#' ${srcdir}/debian/patches/series); do
        patch -p1 -i "${srcdir}/debian/patches/${i}"
    done
}

build() {
  cd "$srcdir"/ImageMagick-6.9.7-4
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --with-dejavu-font-dir=/usr/share/fonts/TTF \
    --with-gs-font-dir=/usr/share/fonts/gsfonts \
    PSDelegate=/usr/bin/gs \
    XPSDelegate=/usr/bin/gxps \
    PCLDelegate=/usr/bin/gpcl6 \
	--enable-reproducible-build \
	--prefix=/usr \
	--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
	--docdir=/$(DOC_PKG_PATH) \
	--with-extra-doc-dir=' (on debian system you may install the imagemagick-$(DEB_UPSTREAM_VERSION_MAJOR)-doc package)' \
	--sysconfdir=/etc \
	--with-includearch-dir=/usr/include/$(DEB_HOST_MULTIARCH)/ \
	--mandir=/usr/share/man \
	--infodir=/usr/share/info \
	--with-modules \
	--with-gs-font-dir=/usr/share/fonts/type1/gsfonts \
	--with-magick-plus-plus \
	--with-djvu \
    --with-openjp2 \
	--with-wmf \
	--without-gvc \
	--enable-shared \
	--without-dps \
	--without-fpx \
	--with-perl \
	--with-perl-options='INSTALLDIRS=vendor' \
	--x-includes=/usr/include/X11 \
	--x-libraries=/usr/lib/X11 \
	--without-rsvg
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

#check() (
#  cd "$srcdir"/ImageMagick-6.9.7-4
#  ulimit -n 4096
#  make check
#)

package() {
  cd "$srcdir"/ImageMagick-6.9.7-4
  make DESTDIR="$pkgdir" install

  find "$pkgdir/usr/lib/perl5" -name '*.so' -exec chrpath -d {} +
#  rm "$pkgdir"/etc/$_relname/type-{apple,urw-base35,windows}.xml
#  rm "$pkgdir"/usr/lib/*.la

  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 LICENSE NOTICE

# Split docs
#  mv "$pkgdir/usr/share/doc" "$srcdir/docpkg/usr/share/"

# Harden security policy https://bugs.archlinux.org/task/62785
#  sed -e '/<\/policymap>/i \ \ <policy domain="delegate" rights="none" pattern="gs" \/>' -i "$pkgdir"/etc/ImageMagick-7/policy.xml
}
